# Deriverse

*Written by Lunar `<lunar at derivation.fr>`*

After experimenting WorkAdventure during the Remote Chaos Communication
Congress experience (#rc3), using it for an event, and toying with map
design and code enhancements, it feels it does not exactly fulfill the needs of
organizing a facilitated online event.

Here is what I wish an online spatial video conferencing software would do.
For the time being, this is just a collection of thoughts.

## Non-goals

**Support 1000+ simultaneous users.** This is meant to help organize small
events.

**Support multiple events running in parallel.** One installation supports one event.

**Permanent database.** When it’s over, it’s over.

**Complex privileges.** Like in a physical event, participants are expected to
behave nicely. If they don’t—like if they get on the stage while they should
not—facilitators can kick them out. That’s it. For small events with trusted
relationships, everyone would have the facilitator status.

**Accounts.** Everyone should feel free to reinvent themselves from an event to
the next. There should also be as little as possible administrative duties as
a participant.

**End-to-end encryption.** For the sake of simplicity. Also we assume one event per
installation. That means a group can setup a dedicated server for the duration of
an event. We assume the server to be trusted (as it will provide the JavaScript
code) and therefore we assume HTTPS + DTLS will be enough to protect the
conversations. 

## Versatile audio/video channels

### Encounters

*Use case*: as a participant, I want to talk to people one-on-one, or in a tiny group.

*Implementation*: when my avatar get close enough to another, it connects our respective video streams.
*[WorkAdventure does it right.]*

XXX: Is 4 the best size?

### Support sending multiple streams per participant

*Use case*: as a presenter, I want to be able to stream my face, a window with my
slides, and a window with a demo at the same time. *[No need to support for
multiple cameras at once. If really needed, sharing a window with VLC playing a
“video capture” is enough.]*

### Selecting inputs without reload

*Use case*: as a presenter, I want to be able to switch from a camera looking at my face to a camera looking at my hands during a demonstration, and back again.

### Support multiple simultaneous channels

*Use case*: as a facilitator, I want to be able to do a general voice announcement
that it’s time to get together.

*Use case*: as a participant, I'd like to share reactions with my friends while we are
watching a movie.

*Use case*: as a participant, I want to be able to hear an interpreter's translation
while watching a presentation.

*Implementation*: Layers on the map can define channels. Upon entering a given
channel users get connected to other users present in the same channel. Tiles
can be used to specify if that means connecting audio, video, chat with any
combinations.

*Note*: it probably means the chat will have tabs to select a channel, 
and that channels should have meaningful names.

### Stream directions

*Use case*: as a facilitator, I want to organize talks with a quick presentation,
followed by questions from the audience.

*Use case*: as a map designer, I want to have a stage where people stand when it’s
their turn to present, an audience that can listen and see the presenters, and
microphones where participants could go to ask questions.

*Use case*: as a presenter, I like to have some feedback on what I am saying. It’s
nice if I could see at least a “front row” of participants while presenting (but
not hear them).

*Implementation*: tiles in channel layers can specify if  media are being
received, sent, or both.

### Audio volume design

*Use case*: as an event facilitator, I want to be able to passively listen to
what is said in a break-out session to get a feeling of how the meeting is going.

*Use case*: as a participant, I used the mobility rule to leave a session I was
not interested in; now I’m searching for another session to join, but I'd like
to listen at a distance to a group before properly joining (and potentially
interrupting) the discussion.

*Use case*: as a map designer, I want to recreate the feeling of a sound installation
in a room, where the volume gets louder as you get closer to the speaker.

*Implementation*: tiles in channel layer can have a volume property.

## Access to external web pages

*Use case*: as a participant, I want to be able to write notes together during our meeting.

*Use case*: as a facilitator, I want users to have access to the timetable.

Implementation: layers in the map can trigger websites to open in an iframe.

Note: we should have enough space for the website to be usable, even on small (vertical) devices. Zoom out the map to keep the avatar at the center while using 4/5 of the screen for the website?

XXX: Do we want to support more than one website open on the game at once? Is
there actually a need?  Reading a text together and writing notes while
discussing it would be nice, but maybe this can be supported be having a “pop
up in a new window” button.

## Accessibility

### Support users on devices without a keyboard

Use case: as a participant using a mobile phone, I should be able to navigate the map and
interact with others.

Implementation: support gestures to move the avatar.

### Links to join meetings and stages

Use case: as blind user, I want to participate in meetings that are currently happening without
navigating an avatar.

Note: this also applies to people who don’t have the hardware to support the game.

Implementation: a link layer on the map + an index with the list of links and how many participants are in each session

### Hang out spaces

Use case: as a blind user, I’d like to meet random participants as much as everyone else.

Note: this also applies to people who don’t have the hardware to support the game.

Implementation:

 * A map layer to specify spaces where people can hang out.
 * A web page with a list of these places and a button to move there.
 * Once joined, an avatar would show up on a random unoccupied location of the space.
 * A web page would list people in the space, and who is talking to who. When selecting someone, the avatar would move to get close enough to trigger an encounter.

### Text based avatar customization

Use case: as a blind user, I'd like to know how I will appear to people who will see my avatar.

### Signal to users to use the chat

Use case: as deaf user, I want to signal to other participants to use the chat.

Note: this would also benefit people who want to participate while people around them are asleep.

XXX: how exactly?

### Follow

Use case: as a shy user, it would be nice to stick with a friend to explore and meet new people.

Note: to avoid harassment, you would offer someone to follow you, and they
would have to accept before it happens.

## Support outside inputs

Use case: as a facilitator, I want to be able to put on some music before the meeting begins.

Use case: as a map designer, I want to create a museum with stand emitting
audio/video coming from unattended pre-recorded or live media.

Use case: as a presenter, I want to stream using OBS.

Implementation: it would be good to have some way to signal automated media
provider that at least someone is watching/listening to avoid using processing
power when no-one is watching.

## Registrations

Participant should be given an invitation link. This would be their way in the event for its
duration. It would stop working if they would get banned. Settings like name/avatar are
entered the first time a link is used and re-used afterward. They can be changed at anytime
during the event.

## Anti-harassment/quality of life

### “Silence” zone

XXX: think more about those

### Block another participant

### Mute any streams

### Report to facilitators

### Panic button

Shut down all camera/microphone/screen that are being sent at once.

(Also accessible via a keyboard shortcut.)

### Language preferences

Use case: as a participant who can speak English and French, I’d rather not
have people try to talk to me in German.

Implementation: have users enter their language preferences (including sign
languages), then display matching ones on an encounter.

XXX: This would be nice, but is it really needed?

## Misc. technical stuff

* Maps API should be versioned
* Design as much of the UI using HTML5/CSS/SVG
* Fully internationalized.
* Easy deployment: the idea is that you can setup something quickly before an event, and recycle the server after the event is finished without too much sweat.

## Sustainability

* Ask for donations in the game.
* Supporters get access to non-free avatars. It makes them “stand out” and also lead (by example) others to give donations.
* Charge for map design.

# Similar software

## WorkAdventure

“WorkAdventure is a video-conference application that lets people hold multiple
parallel conversations in a virtual universe.”

Open source, not really free software. Does not use a SFU.

* [Website](https://work.adventu.re/)
* [Source code](https://github.com/thecodingmachine/workadventure)
* [Demo](https://play.workadventu.re/)

## gather.town

“Better spaces to gather around”

Proprietary.

* [Website](https://gather.town/)

## Calla

Spatialized voice chat built on top of Jitsi Meet. Free software.

* [Source code](https://github.com/capnmidnight/Calla)
* [Website](https://www.calla.chat/)
* [Demo](https://www.calla.chat/Game)
